"""
Project name: houndfox
Module name: forms.py
Description: Definition of the forms used to gather data from the
             user in the application

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""

from django import forms
from django.contrib.auth.models import User
from models import Move
from django.core.validators import MaxValueValidator, MinValueValidator

###############################################################
# Class: UserForm
# Description: Definition of the form used for the registration
#              of users, consisting only of username and password.
# Use: Registration of users
# Author: Pedro Jose Cazorla Garcia
###############################################################
class UserForm(forms.ModelForm):

    # Field for the username.
    # We include some help so that the user knows what input is expected.
    username = forms.CharField()

    # Field for the pasword.
    password = forms.CharField(widget=forms.PasswordInput())

    # We state the fields of the form and the model it refers to.
    class Meta:
        model = User
        fields = ('username', 'password')

###############################################################
# Class: MoveForm
# Description: Definition of the form used for the input
#              of movements, consisting of origin and target.
# Use: Movement in the game
# Author: Beatriz Romera de Blas
###############################################################
class MoveForm(forms.ModelForm):

    # Field for the origin.
    # We include validators so that the number is between 0 and 63, as it is expected.
    origin = forms.IntegerField(validators=[MaxValueValidator(63), MinValueValidator(0)], help_text='origin')

    # Field for the target
    target = forms.IntegerField(validators=[MaxValueValidator(63), MinValueValidator(0)], help_text='target')

    # We state the fields of the form and the model it refers to.
    class Meta:
        model =  Move
        fields = ('origin', 'target')