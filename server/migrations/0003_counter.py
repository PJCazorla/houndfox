# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-11-15 17:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0002_auto_20161025_1800'),
    ]

    operations = [
        migrations.CreateModel(
            name='counter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('counter', models.IntegerField(default=0)),
            ],
        ),
    ]
