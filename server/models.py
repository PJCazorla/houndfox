"""
Project name: houndfox
Module name: models.py
Description: Definition of the models for the database, where
             we will store the information of the application.

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""

from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

###############################################################
# Class: Game
# Description: Information about a game of HoundFox, following
#              the model proposed in the assignment, without
#              any significant changes.
# Author: Beatriz Romera de Blas
###############################################################
class Game(models.Model):

    # Foreign key to Django User model. This is the hound player
    houndUser = models.ForeignKey(User, related_name='game_houndUser')

    # Foreign key to Django User model. This is the fox player
    foxUser = models.ForeignKey(User, related_name='game_foxUser', blank=True, null=True)

    # The following are several integers which represent the position of the board where a particular
    # hound or fox is. For this reason, we introduce some validators so that they are never negative
    # or greater than 63.
    hound1 = models.IntegerField(default=0, validators=[MaxValueValidator(63), MinValueValidator(0)])
    hound2 = models.IntegerField(default=2, validators=[MaxValueValidator(63), MinValueValidator(0)])
    hound3 = models.IntegerField(default=4, validators=[MaxValueValidator(63), MinValueValidator(0)])
    hound4 = models.IntegerField(default=6, validators=[MaxValueValidator(63), MinValueValidator(0)])
    fox = models.IntegerField(default=59, validators=[MaxValueValidator(63), MinValueValidator(0)])

    # Boolean field stating if the turn is the hound's
    houndTurn = models.BooleanField(default=True)

###############################################################
# Class: Move
# Description: Information about a move in the game of HoundFox,
#              following the  simple model proposed in the assignment
# Author: Pedro Jose Cazorla Garcia
###############################################################
class Move(models.Model):

    # Integer between 0 and 63 representing the initial position of the movement.
    origin = models.IntegerField(default=0, validators=[MaxValueValidator(63), MinValueValidator(0)])

    # Integer between 0 and 63 representing the final position of the movement.
    target = models.IntegerField(default=0, validators=[MaxValueValidator(63), MinValueValidator(0)])

    # Foreign key to the class Game, informing the game this move belongs to.
    game = models.ForeignKey(Game)

###############################################################
# Class: Counter
# Description: Simple integer counter stored in the database,
#              which is useful to count the (global) counter
#              needed in the assignment.
# Author: Beatriz Romera de Blas
###############################################################
class Counter(models.Model):
    # The default value is 0, since when this is created, the
    # function hasn't ever been called.
    counter = models.IntegerField(default=0)
