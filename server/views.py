"""
Project name: houndfox
Module name: views.py
Description: Views module of the application 'server'. Each
             function represents a webpage of the application,
             with a given functionality.

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""
from django.shortcuts import render
from models import Counter, User, Game
from forms import UserForm, MoveForm
from test_query import createGame, findFreeGame, makeMove
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout

###############################################################
# Function: index
# Description: Function for the main page. Since it is only
#              a page to display links to the other pages (and,
#              hence, to call other functions), it has no
#              context.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas
###############################################################
def index(request):
    context_dict = {}
    if 'userid' in request.session:
        context_dict['username'] = User.objects.get(id=request.session['userid']).username
        context_dict['hound'] = request.session['amIhound']

    return render(request, 'server/index.html', context=context_dict)

###############################################################
# Function: counterSession
# Description: Function for the counter. Two different counters
#              are used, a session one (in a session variable)
#              and a global one, stored in the database.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas
###############################################################
def counterSession(request):

    # We initialise the context dictionary as empty.
    context_dict = {}

    # Session counter

    # If there is no previous counter, this is the first time we
    # call this function in the session, so we set the var to 1.
    if 'counterSes' not in request.session:
        request.session['counterSes'] = 1

    # Otherwise, we simply increment its value.
    else:
        request.session['counterSes'] = request.session['counterSes'] +1;

    # Global counter

    # There should only be a Counter object in the system, so it is safe
    # to assume the id is 1.
    try:
        # If it exists, we may fetch it.
        ctr = Counter.objects.get(id=1)
    except:
        # Otherwise, we create it, initialise it to 0, and store it in the
        # database.
        ctr = Counter(id=1, counter=0)
        ctr.save()

    # In any case, the global counter is incremented and saved in the database.
    ctr.counter = ctr.counter+1;
    ctr.save()

    # We set up the context variables for the template.
    context_dict['counterGlobal'] = ctr.counter
    context_dict['counterSes'] = request.session['counterSes']

    #if request.is_ajax():
    #    template = "server/game_ajax_v2.html"
    #else:
    #    template = "server/game.html"

    return context_dict

    #return render(request, template, context=context_dict)

###############################################################
# Function: register_user
# Description: Function for registering a user. It has a double
#              functionality. Firstly, to display the form to
#              input data, and secondly, to process it when it
#              has been sent.
# Parameters: request -> HTTP Request.
#                        (If method is post, it has the information
#                         of username and password)
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Pedro Jose Cazorla Garcia.
###############################################################
def register_user(request):

    context_dict = {}

    # If the method is a POST, the information has been sent back
    # from the form, so we should process it.
    if request.method == 'POST':

        # We get the data from the form.
        user_form = UserForm(data=request.POST)

        # If the data is valid, we may finish the registration.
        if user_form.is_valid():

            # Firstly, we save the details of the user.
            user = user_form.save()

            # Secondly, we use the method 'set_password' to encrypt it.
            user.set_password(user.password)

            # And, we save the information to the database.
            user.save()

            # Update our variable to tell the template registration was successful.
            registered = True

        # If there are some errors, we show them to the user, so that they can be
        # solved.
        else:
            context_dict['errors'] = user_form.errors

            # We update the variable, since there wasn't a successful registration.
            registered = False

    # If the method is not POST, we should display the form for the
    # user to fill it up.
    else:
        # We create a UserForm, with the details to be filled up by the user.
        user_form = UserForm()

        # There hasn't been any registration.
        registered = False

    context_dict['user_form'] = user_form
    context_dict['registered']  = registered

    # Render the template depending on the context.
    return render(request, 'server/register.html', context_dict)

###############################################################
# Function: login_user
# Description: Function for logging in a user. It has a double
#              functionality. Firstly, to display the form to
#              input data, and secondly, to process it when it
#              has been sent.
# Parameters: request -> HTTP Request.
#                        (If method is post, it has the information
#                         of username and password)
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas
###############################################################
def login_user(request):

    # We initialise the context dictionary as empty.
    context_dict = {}

    # If the method is a POST, the information has been sent back
    # from the form, so we should process it.
    if request.method == 'POST':

        # We get the username and password from the POST request
        username = request.POST.get('username')
        password = request.POST.get('password')

        # We use Django's framework to check if the combination
        # username/password is valid
        user = authenticate(username=username, password=password)

        # If it is a valid combination...
        if user:
            # ... we check if the user is active.
            if user.is_active:

                # If the account is valid and active, we can log the user in.
                login(request, user)

                # We set user to be the current user, for the template.
                context_dict['user'] = user

                # And we create a session variable containing the ID of the logged user.
                request.session['userid'] = user.id

                if request.POST.get('player_type') == 'hound':
                    return create_game(request)
                else:
                    return join_game(request)

        else:
            context_dict['error'] = 'The user/password combination is invalid'

    # In any case, we render the response with the proper context_dictionary.
    return render(request, 'server/login.html', context_dict)

###############################################################
# Function: logout_user
# Description: Function for logging a user out.
# Restriction: The user must previously have logged into the
#              system.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Pedro Jose Cazorla Garcia
###############################################################
@login_required
def logout_user(request):

    # We initialise an empty context dictionary
    context_dict = {}

    # We get the username with the help of the session variable (ID) and the database.
    context_dict['user'] = User.objects.get(id=request.session['userid']).username

    # And we log the user out.
    logout(request)

    # Finally, we render the page with its context.
    return render(request, 'server/logout.html', context_dict)

###############################################################
# Function: create_game
# Description: Function for creating a game. It also sets up
#              some session variable, which will be useful
#              for other functions.
# Restriction: The user must previously have logged into the
#              system.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Pedro Jose Cazorla Garcia
###############################################################
@login_required
def create_game(request):

    # We initialise an empty context dictionary.
    context_dict = {}

    # We get the User object associated with the logged user.
    user = User.objects.get(id=request.session['userid'])

    # We create a game with the current user as houndUser.
    gm = createGame(user)

    # We set up session variables, which will be useful in later functions.
    # In particular, we store the current gameID
    request.session['gameID'] = gm.id

    # and the fact whether the user is hound in that game or not.
    request.session['amIhound'] = True

    # Finally, we map the variables to the corresponding context dictionary
    # parameters.
    context_dict['user'] = user
    context_dict['game'] = gm
    context_dict['hound'] = True
    context_dict['username'] = user.username

    request.session['lookingForGame'] = False

    # and render the page.
    return render(request, 'server/game.html', context_dict)

###############################################################
# Function: no_logged
# Description: Auxiliary function to display an error message
#              when the user should be logged into the system,
#              but isn't
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas.
###############################################################
def nologged(request):
    # There is no context, so we simply render the page.
    return render(request, 'server/nologged.html', {})

###############################################################
# Function: clean_orphan_games
# Description: Function that deletes all the 'orphan' games
#              of the system, that is, the games without a
#              foxUser.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas.
###############################################################
def clean_orphan_games(request):

    # We initialise an empty context dictionary.
    context_dict = {}

    # We get the list of games without a foxUser.
    gm_list = Game.objects.filter(foxUser__isnull=True)

    # And delete them, getting the number of objects deleted.
    n_pages = gm_list.delete()

    # We map this number to the context dictionary
    context_dict['rows_count'] = n_pages[0]

    # And render the final page.
    return render(request, 'server/clean.html', context_dict)

###############################################################
# Function: join_game
# Description: Function for joining the last game previously
#              created (as a fox user). It also sets up
#              some session variables, if successful.
# Restriction: The user must previously have logged into the
#              system.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Pedro Jose Cazorla Garcia
###############################################################
@login_required
def join_game(request):

    # We initialise an empty context dictionary
    context_dict = {}

    context_dict['username'] = User.objects.get(id=request.session['userid']).username

    # We look for a free game (i.e. game without fox user)
    gm = findFreeGame()

    request.session['amIhound'] = False

    # If there is a free game, we join it.
    if gm:

        # The fox user of the game is the current user, which we
        # retrieve via the database.
        gm.foxUser = User.objects.get(id=request.session['userid'])

        # We save the changes of the game into the database.
        gm.save()

        # We store the game ID in a session variable
        request.session['gameID'] = gm.id

        request.session['lookingForGame'] = False
        # And we also store the fact that we are not hound.

        # We set up the context variables as appropiate.
        #context_dict['thereIsGame'] = True
        #context_dict['game'] = gm

    # If there isn't a game, we set the context parameters as
    # appropiate.
    else:
        # There is no game
        request.session['lookingForGame'] = True

        # We set up an error message.
        #context_dict['error'] = "There is no game available"

    context_dict['hound'] = False
    return render(request, 'server/game.html', context_dict)

###############################################################
# Function: hound_move
# Description: Auxiliary function to be called when the hound
#              user of a game wants to perform a move. It
#              validates the movement and, if correct, makes it.
#
# Parameters: request -> HTTP Request.
#                        (It should contain the origin and the
#                         target in a POST method)
# Restriction: This function should ONLY be called when registered
#              as a hound user.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas
###############################################################
def hound_move(request):

    # We initialise the empty context dictionary.
    context_dict = {}

    # We retrieve the game of the session
    gm = Game.objects.get(id=request.session['gameID'])

    # Using the request, we can get the origin and the target the
    # user wrote into the form.
    origin = request.POST.get('origin')
    target = request.POST.get('target')

    # We use an auxiliary function to check if the hound movement
    # is valid, and get an error/confirmation message.
    # NB: origin and target are strings, so we have to cast them
    # into integers.
    valid, text = houndMoveIsValid(gm, int(origin), int(target))

    # If the movement is not valid,
    if not valid:
        # We cannot perform the move.
        moveDone = False

        # And we create a generic error message, followed by the specific error message
        # of this particular case.
        context_dict['error'] = "Cannot create a move, since it is an invalid move" + text

    # Otherwise, the movement is valid.
    else:
        # So we make the movement with the help of an auxiliary function.
        mv = makeMove(gm, int(origin), int(target))

        # set the corresponding variable to true
        moveDone = True

        # And map some parameters to the context dictionary
        context_dict['move'] = mv
        context_dict['game'] = gm

    # In any case, the fact of whether the move was done is in the variable
    # moveDone.
    context_dict['moveDone'] = moveDone

    # And we render the response.
    return render(request, 'server/move.html', context_dict)

###############################################################
# Function: houndMoveIsValid
# Description: Auxiliary function that checks if a move in a
#              game would be valid for a hound. It doesn't
#              make the move or change the database in any way.
#
# Parameters: game -> Game object where we want to check the move
#             origin -> Origin position (range [0,63])
#             target -> Target position (range [0,63])
# Restriction: This function should ONLY be called when registered
#              as a hound user.
# Returns: 1.- Boolean containing if the move is valid.
#          2.- String with an error, or OK if it is valid.
# Author: Beatriz Romera de Blas
###############################################################
def houndMoveIsValid (game, origin, target):

    if origin > 63 or origin < 0 or target > 63 or target < 0:
        return False, 'Erroneous input'

    # If there is no hound in the origin position, the movement cannot be done.
    if (game.hound1 != origin and game.hound2 != origin and game.hound3 != origin
     and game.hound4 != origin):
        return False, ': You are not in that position'

    # Otherwise, if there is a hound in the target (including the moving one) the
    # movement cannot be done.
    if (game.hound1 == target or game.hound2 == target or game.hound3 == target or
        game.hound4 == target):
        return False, ': There cannot be a hound in the target'

    # Same if the fox is in the target.
    if(game.fox == target):
        return False, ': There cannot be a fox in the target'

    # Otherwise, we only need to check that the movement is correctly made
    # diagonally. For this purpose, we retrieve the row and column of the
    # origin and the destiny.
    # Row: 1+(integer division of position/8)
    # Column: 1+(remainder of the previous division)
    row_origin = origin // 8 + 1
    column_origin = origin % 8 + 1

    row_target = target // 8 + 1
    column_target = target % 8 + 1

    # The movement must be forwards, so the target row should be the next one.
    if row_target != row_origin+1:
        return False, ': you must move to a contiguous diagonal place (forwards)'

    # The movement must be diagonal, so the target column must be either the previous
    # or the next one.
    # NB: If we are in the first or last column, the condition would be (for instance)
    # column_target != -1, which is always true.
    if (column_target != column_origin -1 and column_target != column_origin +1):
        return False, ': you must move to a contiguous diagonal place (forwards)'

    # In any other case, the movement is fine.
    return True, "OK"

###############################################################
# Function: fox_move
# Description: Auxiliary function to be called when the fox
#              user of a game wants to perform a move. It
#              validates the movement and, if correct, makes it.
#
# Parameters: request -> HTTP Request.
#                        (It should contain the origin and the
#                         target in a POST method)
# Restriction: This function should ONLY be called when registered
#              as a fox user.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Pedro Jose Cazorla Garcia
###############################################################
def fox_move(request):

    # We initialise the empty context dictionary.
    context_dict = {}

    # We retrieve the game of the session
    gm = Game.objects.get(id=request.session['gameID'])

    # Using the request, we can get the origin and the target the
    # user wrote into the form.
    origin = request.POST.get('origin')
    target = request.POST.get('target')

    # We use an auxiliary function to check if the fox movement
    # is valid, and get an error/confirmation message.
    # NB: origin and target are strings, so we have to cast them
    # into integers.
    valid, text = foxMoveIsValid(gm, int(origin), int(target))

    # If the movement is not valid,
    if not valid:
        # we cannot perform the move
        moveDone = False

        # And we create a generic error message, followed by the specific error message
        # of this particular case.
        context_dict['error'] = "Cannot create a move, since it is an invalid move" + text

    # Otherwise, the movement is valid.
    else:

        # So we make the movement with the help of an auxiliary function.
        mv = makeMove(gm, int(origin), int(target))

        # set the corresponding variable to true
        moveDone = True

        # And map some parameters to the context dictionary
        context_dict['move'] = mv
        context_dict['game'] = gm

    # In any case, the fact of whether the move was done is in the variable
    # moveDone.
    context_dict['moveDone'] = moveDone

    # And we render the response.
    return render(request, 'server/move.html', context_dict)

###############################################################
# Function: foxMoveIsValid
# Description: Auxiliary function that checks if a move in a
#              game would be valid for a fox. It doesn't
#              make the move or change the database in any way.
#
# Parameters: game -> Game object where we want to check the move
#             origin -> Origin position (range [0,63])
#             target -> Target position (range [0,63])
# Restriction: This function should ONLY be called when registered
#              as a fox user.
# Returns: 1.- Boolean containing if the move is valid.
#          2.- String with an error, or OK if it is valid.
# Author: Pedro Jose Cazorla Garcia
###############################################################
def foxMoveIsValid (game, origin, target):

    if origin > 63 or origin < 0 or target > 63 or target < 0:
        return False, 'Erroneous input'

    # If there is no fox in the origin position, the movement cannot be done.
    if game.fox != origin:
        return False, ': You are not in that position'

    # Otherwise, if there is a hound in the target the movement cannot be done
    if (game.hound1 == target or game.hound2 == target or game.hound3 == target or
                    game.hound4 == target):
        return False, ': There cannot be a hound in the target'

    # Same if the moving fox is already in the target.
    if(game.fox == target):
        return False, ': There cannot be a fox in the target'

    # Otherwise, we only need to check that the movement is correctly made
    # diagonally. For this purpose, we retrieve the row and column of the
    # origin and the destiny.
    # Row: 1+(integer division of position/8)
    # Column: 1+(remainder of the previous division)
    row_origin = origin // 8 + 1
    column_origin = origin % 8 + 1

    row_target = target // 8 + 1
    column_target = target % 8 + 1

    # The movement must be diagonal, so the target row should be either
    # the next one or the previous one.
    if (row_target != row_origin+1 and row_target != row_origin-1):
        return False, ': you must move to a contiguous diagonal place'

    # The movement must be diagonal, so the target column must be either the previous
    # or the next one.
    # NB: If we are in the first or last column, the condition would be (for instance)
    # column_target != -1, which is always true.
    if (column_target != column_origin -1 and column_target != column_origin +1):
        return False, ': you must move to a contiguous diagonal place'

    # In any other case, the movement is fine.
    return True, 'OK'

###############################################################
# Function: move
# Description: Function for making a move in the current game,
#              regardless of whether the user is a hound or a
#              fox.
#              It has a double functionality, since it also
#              displays the form for the user to fill in the data.
# Restriction: The user must previously have logged into the
#              system.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Pedro Jose Cazorla Garcia
###############################################################
@csrf_exempt
@login_required
def move(request):

    # We initialise the empty context dictionary.
    context_dict = {}

    # If the method is a post, we already have the information we need.
    if request.method == 'POST':

        # If it is currently the houndturn and the user is a hound, then the
        # movement is managed by the auxiliary function hound_move.
        if (Game.objects.get(id=request.session['gameID']).houndTurn and
            request.session['amIhound']):
            return hound_move(request)

        # If the user is a fox, and it is the fox's turn, then it is managed by
        # fox_move.
        elif (not request.session['amIhound'] and not Game.objects.get(id=request.session['gameID']).houndTurn):
            return fox_move(request)

        # Otherwise, it is simply not the user's turn, so we cannot move.
        else:
            context_dict['error'] = 'Cannot create a move, since it is not your turn!'

    # Otherwise, we need to send the form, so that the user fills in the data.
    else:
        # We create the form.
        move_form = MoveForm()

        # And map it to the context dictionary.
        context_dict['move_form'] = move_form

    # If we have got here, then the move cannot be done, for some reason.
    context_dict['moveDone'] = False

    # And we render the response.
    return render(request, 'server/move.html', context_dict)

###############################################################
# Function: status_turn
# Description: Function for checking if it is currently the
#              user turn.
# Restriction: The user must previously have logged into the
#              system.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas
###############################################################
@login_required
def status_turn(request):

    # We initialise the context dictionary empty.
    context_dict = {}

    # If the turn in the current game is the hound's and the user is hound, then it is
    # his/her turn
    if Game.objects.get(id=request.session['gameID']).houndTurn and request.session['amIhound']:
        myTurn = True

    # Same if the turn is the fox's and the user is fox.
    elif (not Game.objects.get(id=request.session['gameID']).houndTurn) and (not request.session['amIhound']):
        myTurn = True

    # In any other case, it is not his/her turn
    else:
        myTurn = False

    # We map the variable to the context dictionary
    context_dict['myTurn'] = myTurn

    # And we render the response.
    return render(request, 'server/turn.html', context_dict)

def my_status_turn(request):

    # We initialise the context dictionary empty.
    context_dict = {}

    # If the turn in the current game is the hound's and the user is hound, then it is
    # his/her turn
    if Game.objects.get(id=request.session['gameID']).houndTurn and request.session['amIhound']:
        myTurn = True

    # Same if the turn is the fox's and the user is fox.
    elif (not Game.objects.get(id=request.session['gameID']).houndTurn) and (not request.session['amIhound']):
        myTurn = True

    # In any other case, it is not his/her turn
    else:
        myTurn = False

    return myTurn

###############################################################
# Function: status_board
# Description: Function for representing graphically the
#              status of the board in a given time. For this
#              purpose, an array is returned where 1 represents
#              a hound, -1 a fox and 0 none of them.
# Restriction: The user must previously have logged into the
#              system.
# Parameters: request -> HTTP Request.
# Returns: The rendered HTML object with the corresponding
#          template.
# Author: Beatriz Romera de Blas
###############################################################
@login_required
def status_board(request):

    context_dict = getstatus_board(request)

    # And render the response.
    return render(request, 'server/board.html', context_dict)

def getstatus_board(request):

    # We initialise the context dictionary
    context_dict = {}

    context_dict['username'] = User.objects.get(id=request.session['userid']).username

    # We try to get the game object.
    try:
        # We get the game object of the session.
        gm = Game.objects.get(id=request.session['gameID'])

        # We initialise the array as 64 zeros.
        array = [0] * 64

        # And we set to 1 the positions where there are hounds.
        array[gm.hound1] = array[gm.hound2] = array[gm.hound3] = array[gm.hound4] = 1

        # And we set to -1 the position of the fox.
        array[gm.fox] = -1

    # If there is an exception, there was no game object, so the array must be
    # empty, so that the user is informed of this.
    except:
        array = []

    # We map the array variable to the context dictionary.
    context_dict['board'] = array

    return context_dict

@login_required
def game(request):
    context_dict = {}

    if 'userid' in request.session:
        context_dict['username'] = User.objects.get(id=request.session['userid']).username
        context_dict['hound'] = request.session['amIhound']

    return render(request, 'server/game.html', context_dict)

def check(request):

    context_dict = {}

    context_dict['username'] = User.objects.get(id=request.session['userid']).username

    gm = findFreeGame()

    if request.session['lookingForGame'] and gm:
        gm.foxUser = User.objects.get(id=request.session['userid'])
        gm.save()
        request.session['gameID'] = gm.id
        request.session['lookingForGame'] = False


    if request.session['amIhound'] and not Game.objects.get(id=request.session['gameID']).foxUser:
        context_dict['msg'] = "Waiting for a fox"
    elif request.session['lookingForGame']:
        context_dict['msg'] = "Waiting for a free game"
    elif my_status_turn(request):
        context_dict['turn'] = True
        context_dict['msg'] = "It is your turn"
        context_dict['board'] = (getstatus_board(request))['board']
    else:
        context_dict['turn'] = False
        context_dict['msg'] = "It is not your turn"
        context_dict['board'] = (getstatus_board(request))['board']

    return render(request, 'server/check.html', context_dict)

def check2(request):
    context_dict = {}

    gm = Game.objects.filter().order_by('-id')[0]
    context_dict['board'] = getBoard(gm)

    return render(request, 'server/check.html', context_dict)

def houndswin(game):

    fox_origin = game.fox

    b1, m1 = foxMoveIsValid(game, fox_origin, (fox_origin+7)%64)
    b2, m2 = foxMoveIsValid(game, fox_origin, (fox_origin+9)%64)
    b3, m3 = foxMoveIsValid(game, fox_origin, (fox_origin-7)%64)
    b4, m4 = foxMoveIsValid(game, fox_origin, (fox_origin-9)%64)


    if b1 or b2 or b3 or b4:
        return False

    return True

def foxwins(game):

    fox_row = game.fox // 8

    if fox_row == 0:
        return True

    return False

def gameEnd(request):

    game = Game.objects.get(id=request.session['gameID'])
    context_dict = {}

    if (houndswin(game) and request.session['amIhound']) or (foxwins(game) and not request.session['amIhound']):
        msg = "You won the Game!!!!"
        win = True
    elif (foxwins(game) and request.session['amIhound']) or (houndswin(game) and not request.session['amIhound']):
        msg = "You lost..."
        win = False
    else:
        msg = "Not finished"
        win = False

    context_dict['msg'] = msg
    context_dict['win'] = win

    return render(request, 'server/end.html', context_dict)

def show(request):
    context_dict = {}
    if 'userid' in request.session:
        context_dict['username'] = User.objects.get(id=request.session['userid']).username
        context_dict['hound'] = request.session['amIhound']

    return render(request, 'server/show.html', context_dict)

def gameEnd2(request):

    context_dict = {}

    gm_list = Game.objects.filter().order_by('-id')

    # If there is any of these games, we return the first (newest)
    if gm_list.exists():
        game = gm_list[0]
        if houndswin(game) or foxwins(game):
            context_dict['msg'] = "That was a truly amazing game! If you like it, go play it!"
            context_dict['win'] = True

        else:
            context_dict['msg'] = "Not finished"

    return render(request, 'server/end.html', context_dict)

def getBoard(game):

    # We initialise the array as 64 zeros.
    array = [0] * 64

    # And we set to 1 the positions where there are hounds.
    array[game.hound1] = array[game.hound2] = array[game.hound3] = array[game.hound4] = 1

    # And we set to -1 the position of the fox.
    array[game.fox] = -1

    return array

