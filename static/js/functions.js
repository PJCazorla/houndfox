var myRefresh;
var lastUsed = -1;
var finished = false;

function refresh() {

    $.ajax({

        url: '/server/end/',

        success: function (data) {

            if(data.indexOf("Not") == -1){
                clearTimeout(myRefresh);
                $('#game').html(data);
                finished = true;
            } else {
                finished = false;
            }
            }
        })


    if(!finished){
        $.ajax({

        url: '/server/check/',

        success: function (data) {
            // ...and it will insert it inside #result element
            $('#board').html(data);

            if(!finished){
                myRefresh = setTimeout(refresh, 2000);
            }

            }
        })
    }

}

function refresh2() {

    $.ajax({

        url: '/server/end2/',

        success: function (data) {

            if(data.indexOf("Not") == -1){
                clearTimeout(myRefresh);
                $('#game').html(data);
            }
            }
    })

    $.ajax({

    url: '/server/check2/',

    success: function (data) {
        // ...and it will insert it inside #result element
        $('#board').html(data);
        myRefresh = setTimeout(refresh2, 1000);
        }
    })

}

function getID(oObject)
{
    clearTimeout(myRefresh);

    var id = oObject.id;

    id = id.substr(3);

    if(lastUsed != -1){

        $.ajax({
          type: "POST",
          url: "/server/move/", // this should grab only #table element...
          data: {
              'origin':lastUsed,
              'target':id,
             // 'csrfmiddlewaretoken': Cookies.get('csrftoken') // security_token
          },
         success: function (data) {

            if(data.indexOf("ERROR") != -1){
                $('#error').html(data);
            } else{
                // ...and it will insert it inside #result element
                refresh();
            }
         }
        })
        $('.selected').removeClass("selected");
        lastUsed = -1;

    }
    else {
        $('#error').html("");
        lastUsed = id;
        oObject.className = "selected";
    }
  }

function deleteTimeout(){
    clearTimeout(myRefresh);
    }