# Setup for local utilisation
dropdb -U alumnodb -h localhost houndfox
createdb -U alumnodb -h localhost houndfox
export DATABASE_URL="postgres://alumnodb:alumnodb@localhost:5432/houndfox"
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser 
