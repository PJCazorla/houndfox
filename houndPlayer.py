# lucnh this script and as mouse first clean orphan games and then
# join the game
import os,django
os.environ['DJANGO_SETTINGS_MODULE'] =  'houndfox.settings'
django.setup()
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test import Client
from server.models import Game, Move, Counter
import time
import re, math
import random, json
from server.views import houndMoveIsValid
#python ./manage.py test rango.tests.UserAuthenticationTests --keepdb
#class UserAuthenticationTests(TestCase):

usernameCat = 'gatoUser'
passwdCat = 'gatoPasswd'
usernameMouse = 'ratonUser'
passwdMouse = 'ratonPasswd'
DEBUG = False

def foxMoveIsGood(origin, target, houndPosition):
    row_hound = []
    n_behind_before = 0
    n_behind_after = 0

    # A movement is really good if it leaves behind any hounds
    for h in houndPosition:
        if h // 8 >= origin // 8:
            n_behind_before += 1
        if h // 8 >= target // 8:
            n_behind_after += 1

    if n_behind_after > n_behind_before:
        return 2

    # A movement is good if it goes forwards
    if target // 8 < origin // 8:
        return 1

    # Otherwise, the movement is bad
    return 0

class CatPlayer():
    def __init__(self):
        self.clientCat = Client()
        self.cats = [0, 2, 4, 6]
        self.game=None
        currenTime = time.time()
        RandomSeed = int(currenTime) + os.getpid()
        random.seed(RandomSeed)

    def creatUser(self, userName, userPassword):
        try:
            user = User.objects.get(username=userName)
        except User.DoesNotExist:
            user = User(username=userName, password=userPassword)
            user.set_password(user.password)
            user.save()
        return user.id

    def login(self, userName, userPassword, client):
        response = client.get(reverse('login_user'))
        loginDict={}
        loginDict["username"]=userName
        loginDict["password"]=userPassword
        response = client.post(reverse('login_user'), loginDict, follow=True)
        return response


    def deleteUser(self, userName):
        try:
            userKK = User.objects.get(username=userName)
            userKK.delete()
        except User.DoesNotExist:
            pass

    def logout(self, client):
        try:
            response = client.get(reverse('logout_user'), follow=True)
            return response
        except:
            pass

    def wait_loop(self, seconds):
        key = "It is your turn : True"
        print ("wait loop begin")
        while True:
            response = self.clientCat.get(reverse('status_turn'))
            if response.content.find(key) != -1:
                break
            time.sleep(seconds)
            print ".",
        print ("wait loop end")
        return 0

    def parse_mouse(self):
        """http://pythex.org/"""
        response = self.clientCat.get(reverse('status_board'))
        searchString='<td id=id_(\d+) style=\'width: 20px\'>\s+&#9920;'
                       #<td id=id_0 onclick="getID(this);" >\n              &#9922;
        finds = re.findall(searchString,response.content)
        print finds
        return finds[0]

    def validRandomMove(self,mousePosition):
        counter=0
        validMoves=[+7,+9]
        target = -1
        catIndex = -1
        origin = -1
        moveDict={}

        while True:
            counter += 1
            if counter > 1000:
                print "I cannot move. You won."
                exit(0)

            # Use minimax algorith to determine best movement.
            origin, target = self.houndMiniMax(self.cats, mousePosition)

            print("origin target cats", origin, target, self.cats)
            #cannot be a cat
            if mousePosition == target:
                 print("continue mouseposition", mousePosition,target )
                 continue
            #canot be a cat in target
            continueit=False
            for cat in self.cats:
                if cat == target:
                    print("continue cats")
                    continueit = True
            if continueit:
                continue
            #can not move two rows o no row
            if not ( 1 + (origin//8)) ==(target//8) :
                 print("continue no two rows")
                 continue
            # if target in the right range break
            if target < 0 or target > 63:
                print("continue target self.cats, target ",self.cats, target)
                continue

            moveDict['origin']=origin
            moveDict['target']=target
            response = CatPlayer.clientCat.post(reverse('move'), moveDict)
            #return render(request, 'server/move.html', moveDict)
            #CHANGE THIS
            #return HttpResponse(json.dumps({'error':error, 'moveDone':moveDone}),
            #        content_type="application/json"
            #    )

            #esponseDict = json.loads(response.content)
            if 'ERROR' in response.content:
                continue
            else:
                break
        return origin,target
        #try to move
    def set_catPosition(self,origin, position):
        for cat in range(0,4):
            if self.cats[cat] == origin:
                self.cats[cat] = target
                break
        print("self.cats",self.cats)

    # Heuristic for a hound movement
    def houndMoveisGood(self, hounds_target, foxPosition):

        foxMoves=[-7,-9,+7,+9]

        # A movement is really good if it blocks the fox forward
        if foxPosition-7 in hounds_target and foxPosition-9 in hounds_target:
            return 2

        foxScores = []

        # Otherwise, it depends on how good the subsequent fox movement is:
        for i in range(0,4):
            foxScores.append(foxMoveIsGood(foxPosition, foxPosition + foxMoves[i], hounds_target))

        return 2-max(foxScores)

    def foxTwoRows(self, fox):
        col = fox%8
        mov_list = []
        mov_list.append(fox - 16)
        if col==6 or col==7:
            mov_list.append(fox - 18)
        elif col==0 or col==1:
            mov_list.append(fox-14)
        else:
            mov_list.append(fox-18)
            mov_list.append(fox-14)

        return mov_list

    def foxOneRow(self, fox):
        col = fox%8
        mov_list = []

        if col==0:
            mov_list.append(fox-7)
        elif col==7:
            mov_list.append(fox-9)
        else:
            mov_list.append(fox-7)
            mov_list.append(fox-9)

        return mov_list

    def houndMiniMax(self, hounds, fox):

        gm = Game.objects.latest("id")

        # We evaluate all possible movements of hounds.
        validMoves=[+7,+9]
        foxMoves = [-7,-9,+7,+9]
        currentMax = -1

        origin = -1
        target = -1

        found = False

        # We look to minimise the maximum performance of the fox
        for i in range(0,4):
            for j in range(0,2):
                if houndMoveIsValid(gm, hounds[i], hounds[i]+validMoves[j])[0]:

                    if int(hounds[i])+validMoves[j] in self.foxTwoRows(int(fox)):
                        origin = hounds[i]
                        target = int(hounds[i])+validMoves[j]
                        found = True

                    elif int(hounds[i])+validMoves[j] in self.foxOneRow(int(fox)):
                        origin = hounds[i]
                        target = int(hounds[i])+validMoves[j]
                        found = True

            # If we didnt succeed, we simply keep the position
        if not found:
            minRow = 100

            for hound in hounds:
                if hound//8 < minRow and (houndMoveIsValid(gm, hound, hound+validMoves[0])[0] or houndMoveIsValid(gm, hound, hound+validMoves[1])[0]):

                    origin = hound

                    # Even rows
                    if hound%2 == 1:
                        target = hound+7

                    # Odd rows
                    else:
                        target = hound+9

                    minRow = hound//8

                    while not houndMoveIsValid(gm, origin, target)[0]:
                        target = hound + validMoves[random.randrange(0,2)]

        return origin, target



CatPlayer = CatPlayer()
#make sure mouse user exists
userCatID = CatPlayer.creatUser(usernameCat, passwdCat)
#login
response = CatPlayer.login(usernameCat, passwdCat,CatPlayer.clientCat)
#create game
response = CatPlayer.clientCat.get(reverse('create_game'))
#get last game id
game = Game.objects.latest("id")

#check if mouse has joined
print("waiting untill mouse joins")
while True:
     time.sleep(1)
     gameOK = Game.objects.filter(foxUser__isnull=True).order_by("-id")
     if gameOK.exists():
         game2 = gameOK[0]
         if game.id == game2.id:
             print ".",
             continue
     else:
         break
print("game id=%d"%game.id)
moveDict={}
while True:
     # loop waiting for myTurn=True
     CatPlayer.wait_loop(1)# time in second between retrial

     #moveDict['origin']=0
     #moveDict['target']=9
     #response = CatPlayer.clientCat.post(reverse('move'), moveDict)

     # parse mouse position
     mousePosition = CatPlayer.parse_mouse()
     print('mousePosition', mousePosition)
     #create random cat move
     origin, target = CatPlayer.validRandomMove(mousePosition)
     #move
     CatPlayer.set_catPosition(origin, target)

