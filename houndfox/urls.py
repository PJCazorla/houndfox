"""
Project name: houndfox
Module name: urls.py
Description: Map of URLs of the project to different application.
             In this case, it is only used for one application, server.

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""

from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    # Administration URLs
    url(r'^admin/', admin.site.urls),

    # Application 'server' URLs
    url(r'^server/', include('server.urls')),

    # Since we only have one application, we set the first page
    # to 'server' as well.
    url(r'^', include('server.urls'))
]

urlpatterns += staticfiles_urlpatterns()
