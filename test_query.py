"""
Project name: houndfox
Module name: test_query.py
Description: Module containing some auxiliary function and a
             main procedure where they are tested, according
             to the requirements of the assignment.

Authors: Pedro Jose Cazorla Garcia and Beatriz Romera de Blas
Date: 27-November-2016

Computer Systems Project, assignment 3
"""
import os , django
os.environ.setdefault( 'DJANGO_SETTINGS_MODULE' ,   'houndfox.settings')
django.setup( )
from server.models import Game, Move
from django.contrib.auth.models import User

###############################################################
# Function: getOrCreate
# Description: Function that, given an id, gets the corresponding
#              user of the database. If it doesn't exist, it
#              creates it with the given username and password,
# Parameters: id -> Integer, id of the user on the database
#             username (optional) -> Username to create the
#                            new user, if needed
#             password (optional) -> Password to create the new
#                            user, if needed.
# Returns: The retrieved or created user.
# Author: Pedro Jose Cazorla Garcia
###############################################################
def getOrCreate(id, username="username", password="password"):

    # We try to get the user with its ID
    try:
        u = User.objects.get(id=id)

    # If we can't,
    except:

        # We create it with the given username.
        u = User(id=id, username=username)
        # And encrypt the password.
        u.set_password(password)

        # Save changes to the database.
        u.save()

    # We return the user.
    return u

###############################################################
# Function: createGame
# Description: Function that, given a User object, creates a game
#              with it as the houndUser.
# Parameters: houndUser -> User object, to be used as houndUser
# Returns: The created game
# Author: Beatriz Romera de Blas
###############################################################
def createGame(houndUser):

    # We create and save the game
    gm = Game(houndUser=houndUser)
    gm.save()

    # And return it.
    return gm

###############################################################
# Function: findFreeGame
# Description: Function that looks for the most recent game
#              without a foxUser (orphan) and returns it, if
#              it exists.
# Parameters: None
# Returns: The latest orphan game, or [] if it doesn't exist.
# Author: Beatriz Romera de Blas
###############################################################
def findFreeGame():

    # We get the list of orphan games, from the most recent to the
    # oldest.
    gm_list = Game.objects.filter(foxUser__isnull=True).order_by('-id')

    # If there is any of these games, we return the first (newest)
    if gm_list.exists():
        return gm_list[0]
    # If there isn't, we return an empty array.
    else:
        return []

###############################################################
# Function: makeMove
# Description: Function that given a game, and two positions,
#              creates and saves a movement with those positions
#              as origin and target. The correction of the movement
#              is not verified here.
# Parameters: game -> Game object
#             origin -> Origin position of the game (range [0,63])
#             target -> Target position of the game (range [0,63])
# Returns: The created move
# Author: Pedro Jose Cazorla Garcia
###############################################################
def makeMove(game, origin, target):

    # We create the move with the given parameters, and save it
    # to the database.
    mv = Move(game=game, origin=origin, target=target)
    mv.save()

    # The turn in the game is reversed
    game.houndTurn = not game.houndTurn

    # We check what hound or fox was on the origin position, in order
    # to know which parameter we should modify.
    if game.hound1 == origin:
        game.hound1 = target
    elif game.hound2 == origin:
        game.hound2 = target
    elif game.hound3 == origin:
        game.hound3 = target
    elif game.hound4 == origin:
        game.hound4 = target
    else:
        game.fox = target

    # Finally, we save the changes and return the move.
    game.save()
    return mv


# If this script is executed as a main script, we want to perform some
# tests.
if __name__ == '__main__':

    # We create two users, with id 10 and 11 respectively.
    u10 = getOrCreate(id=10, username="user10", password="password10")
    u11 = getOrCreate(id=11, username="user11", password="password11")

    # We create a Game, having the first user as a houndUser.
    gm = createGame(u10)

    # We try to find a free game, so that u11 can join it.
    gm2 = findFreeGame()

    # This if is only for completeness, but it is always true.
    if gm2 != []:

        # We set 11 to be the fox user of the game, and save the
        # changes to the database.
        gm2.foxUser = u11
        gm2.save()

        # Hound Move
        makeMove(gm2, 2, 11)

        # Fox move
        makeMove(gm2, 59, 52)

